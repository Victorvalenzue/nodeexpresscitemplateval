const express = require('express');

const apiRouter = express();

apiRouter.use('/user', require('./user'));
apiRouter.use('/data', require('./data'));

module.exports = apiRouter;
