// body parser
const body_parser = require('body-parser');
// middleware
const express = require('express');
const logger = require('./logger/logger');

// const porti = process.env.PORT;
// console.log(`Your port is ${porti}`);

const server = express();

// parse JSON (application/json content-type)
server.use(body_parser.json());

// HTML router handler
server.get('/', (req, res) => {
  res.sendFile(`${__dirname}/index.html`);
});

server.use('/api', require('./routes/routes'));

// request to handle undefined or all other routes
server.get('*', (req, res) => {
  logger.info('users route');
  res.send('App works!!!!!');
});

// Start server
const port = 4000;
server.listen(port, () => {
  logger.info(`Running server on from port:::::::${port}`);
});
